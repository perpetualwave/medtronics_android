package com.perpetualwave.medtronics.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.perpetualwave.medtronics.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by perpetualwave on 23/02/16.
 */
public class FingerprintDialogFragment extends DialogFragment {

    public FingerprintDialogFragment() {
    }

    public static FingerprintDialogFragment newInstance(){
        FingerprintDialogFragment df = new FingerprintDialogFragment();
        return df;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.dialog_fingerprint, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

}
