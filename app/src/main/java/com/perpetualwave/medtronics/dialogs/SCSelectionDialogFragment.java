package com.perpetualwave.medtronics.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.perpetualwave.medtronics.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by perpetualwave on 23/02/16.
 */
public class SCSelectionDialogFragment extends DialogFragment {

    @Bind(R.id.btn_scan_for_inventory_sc_selection)
    View btn_scan_for_inventory_sc_selection;
    @Bind(R.id.btn_return_product_sc_selection)
    View btn_return_product_sc_selection;
    @Bind(R.id.btn_use_product_sc_selection)
    View btn_use_product_sc_selection;

    public SCSelectionDialogFragment() {
    }

    public static SCSelectionDialogFragment newInstance(){
        SCSelectionDialogFragment df = new SCSelectionDialogFragment();
        return df;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.sc_selection, container, false);

        ButterKnife.bind(this, view);

        btn_return_product_sc_selection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                ReturnProductDialogFragment.newInstance().show(getFragmentManager(), "RPDF");
            }
        });

        btn_use_product_sc_selection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                UseProductDialogFragment.newInstance().show(getFragmentManager(), "UPDF");
            }
        });

        return view;
    }

}
