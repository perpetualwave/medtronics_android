package com.perpetualwave.medtronics.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.perpetualwave.medtronics.R;
import com.perpetualwave.medtronics.dialogs.CCSSelectionDialogFragment;
import com.perpetualwave.medtronics.dialogs.SCSelectionDialogFragment;
import com.perpetualwave.medtronics.fragments.DeliveryTrackingFragment;
import com.perpetualwave.medtronics.fragments.HospitalReceiveFragment;
import com.perpetualwave.medtronics.fragments.InventoryListFragment;
import com.perpetualwave.medtronics.fragments.SupplierDeliveryDetailsFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        getFragment(new InventoryListFragment());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_supplier_delivery) {
            getFragment(new SupplierDeliveryDetailsFragment());
        } else if (id == R.id.nav_delivery_tracking) {
            getFragment(new DeliveryTrackingFragment());
        } else if (id == R.id.nav_hospital_receive) {
            getFragment(new HospitalReceiveFragment());
        } else if (id == R.id.nav_central_core) {
            CCSSelectionDialogFragment df = CCSSelectionDialogFragment.newInstance();
            df.show(getSupportFragmentManager(), "CCSDF");
        } else if (id == R.id.nav_sterile_core) {
            SCSelectionDialogFragment df = SCSelectionDialogFragment.newInstance();
            df.show(getSupportFragmentManager(), "SCDF");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void getFragment(Fragment SelectedFragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.container, SelectedFragment);
        fm.popBackStack(null, android.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
        ft.commit();
        if(!(SelectedFragment instanceof InventoryListFragment))
            onBackPressed();
    }
}
