package com.perpetualwave.medtronics.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.perpetualwave.medtronics.R;

/**
 * Created by jakejacobo on 17/05/2016.
 */
public class LoginActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login);

        Button btnSignin = (Button)findViewById(R.id.btnSignIn);
        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callActivity(new Intent(getApplicationContext(), MainActivity.class));
            }
        });


        /*TextView btnSignup = (TextView)findViewById(R.id.signupBtn);
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callActivity(new Intent(getApplicationContext(), SignupActivity.class));
            }
        });*/

    }




    private void  callActivity(Intent intent){
        startActivity(intent);
    }

}
