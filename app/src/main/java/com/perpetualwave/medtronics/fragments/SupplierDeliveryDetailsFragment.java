package com.perpetualwave.medtronics.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.perpetualwave.medtronics.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SupplierDeliveryDetailsFragment extends Fragment {

    @Bind(R.id.btn_view_product_detail_supplier_delivery)
    TextView btn_view_product_detail_supplier_delivery;
    @Bind(R.id.btn_continue_supplier_delivery_detail)
    Button btn_continue_supplier_delivery_detail;

    public SupplierDeliveryDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.supplier_delivery_details, container, false);

        ButterKnife.bind(this, view);
        btn_continue_supplier_delivery_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Delivery", Toast.LENGTH_LONG).show();
            }
        });

        return view;
    }

}
