package com.perpetualwave.medtronics.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.perpetualwave.medtronics.R;
import com.perpetualwave.medtronics.dialogs.FingerprintDialogFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class HospitalReceiveFragment extends Fragment {

    @Bind(R.id.btn_view_product_detail_hospital_receive)
    TextView btn_view_product_detail_hospital_receive;
    @Bind(R.id.btn_continue_hospital_receive)
    Button btn_continue_hospital_receive;

    public HospitalReceiveFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hospital_receive, container, false);
        ButterKnife.bind(this, view);

        btn_continue_hospital_receive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FingerprintDialogFragment.newInstance().show(getFragmentManager(), "FPDF");
            }
        });

        return view;
    }

}
