package com.perpetualwave.medtronics.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.perpetualwave.medtronics.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CCSScanOutFragment extends Fragment {


    public CCSScanOutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.ccs_scan_out_sterile_core, container, false);
    }

}
